repo_url = "gino001d.gitlab.io/"
repo_name = "doggostore"
repo_icon = "fdroid-icon.png"
repo_description = """
Unofficial Firefox repo (.apk files from mozilla.org)
"""

archive_older = 0

local_copy_dir = "/fdroid"

keystore = "../keystore.bks"
repo_keyalias = "rfc2822"
